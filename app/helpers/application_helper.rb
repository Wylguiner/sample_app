module ApplicationHelper

  #Returns the full title on a per-page basis
  def full_title(page_title = '')
    base_title = "Ruby on Rails Tutorial Sample App"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

   def send_push_notification(user, message, title)
    gcm = GCM.new('AAAAOuMUk9w:APA91bFlXO7L4Th73sR5fVVIF-y0XjElgfA2k0Glsy4GtTT-3csrFsLDOj_tVdbKNEsRyPJbSb70WvqQPXMSZF-Y__LzRB0-vEKtuNyQpD-TtVZJbpWovD0FbnuCsZP41c24YvBUkkQR')
    
    registration_ids= user.user_token.map{ |dev| dev.token }

    options = {data: {message: message, title: title}, collapse_key: "do_not_collapse"}
    
    puts gcm.send(registration_ids, options)
  end

end
