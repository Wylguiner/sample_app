class UserTokenController < ApplicationController
  
  def set_token
    @token = UserToken.find_by(token: params[:token])
    if @token
      return false
    else
      @user_token = UserToken.new(user: current_user, token: params[:token])
      if @user_token.save
        render json: true
      else
        render json: @user_token.errors.to_json
      end
    end 
  end

  def send_notification
    send_push_notification(User.find(params[:user_id]), "Teste", "Titulo teste")
  end

  private 

  def send_push_notification(user, message, title)
    gcm = GCM.new('AAAAOuMUk9w:APA91bFlXO7L4Th73sR5fVVIF-y0XjElgfA2k0Glsy4GtTT-3csrFsLDOj_tVdbKNEsRyPJbSb70WvqQPXMSZF-Y__LzRB0-vEKtuNyQpD-TtVZJbpWovD0FbnuCsZP41c24YvBUkkQR')
    
    registration_ids = user.user_token.map{ |dev| dev.token }
    puts registration_ids
    options = { body: message, 
        title: title, 
        icon: 'images/rails.png',
        tag: 'my-tag',
        actions: [
          {action:"like", title: "Like"}, 
          {action:"reply", title: "⤻ Reply"}]
       } 
    gcm.send(registration_ids, options)

  end
end
