class ApplicationController < ActionController::Base
  
  protect_from_forgery with: :exception
  before_action :get_session_time_left
  include SessionsHelper

  def update_activity_time
    session[:expires_at] = 1.minutes.from_now
  end


  private

  # Confirms a logged-in user.
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end

 def get_session_time_left
    expire_time = session[:expires_at] || Time.now
    @session_time_left = (expire_time - Time.now).to_i
  end

end

