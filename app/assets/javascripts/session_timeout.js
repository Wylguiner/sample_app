var sessionTimeout = 1800;
var interval;

function DisplaySessionTimeout() {
    if (interval == null)
        interval = window.setInterval("decrementTimeout()", 1000);
}

function ResetTimeout() {
    sessionTimeout = 1800;
}

function decrementTimeout() {
    sessionTimeout -= 1;
    document.getElementById("contador").innerText = sessionTimeout;
    document.getElementById("contador2").innerText = sessionTimeout;
    $("a[href!='#']").on('click', function () {
        ResetTimeout();
    });
    if (sessionTimeout <= 0) {      
        $("#expire").removeClass('hidden');
        $('#warning').addClass('hidden');
        window.clearInterval(interval);
    } 
    else if (sessionTimeout <= 100) {
        $('.expiration-warning').modal('show');
        $("#warning").removeClass('hidden');
        DisplaySessionTimeout();
    }   

}



